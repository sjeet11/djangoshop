from django.contrib import admin
from .models import AgeGroup, Brand, ShoeType, Product
# Register your models here.


class AgeGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')


class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')


class ShoeTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'shoe_size',
                    'age_group', 'shoe_type', 'brand')


admin.site.register(AgeGroup, AgeGroupAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(ShoeType, ShoeTypeAdmin)
admin.site.register(Product, ProductAdmin)
