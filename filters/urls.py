from django.urls import path
from . import views

app_name = 'shoes'
urlpatterns = [
    path('', views.index, name="index"),
    path('brand/<brand_id>', views.brand_details, name="brand_details"),
    path('group/<group_id>', views.group_details, name="group_details"),
    path('type/<type_id>', views.type_details, name="type_details")
]
