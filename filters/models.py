from django.db import models
from django.utils import timezone

# Create your models here.


class AgeGroup(models.Model):
    name = models.CharField(max_length=255)
    logo = models.CharField(max_length=10000)

    def __str__(self):
        return self.name


class ShoeType(models.Model):
    name = models.CharField(max_length=255)
    logo = models.CharField(max_length=10000)

    def __str__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=255)
    logo = models.CharField(max_length=10000)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    shoe_size = models.CharField(max_length=15)
    img1_url = models.CharField(max_length=10000)
    img2_url = models.CharField(max_length=10000)
    img3_url = models.CharField(max_length=10000)
    img4_url = models.CharField(max_length=10000)
    age_group = models.ForeignKey(AgeGroup, on_delete=models.CASCADE)
    shoe_type = models.ForeignKey(ShoeType, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    time_created = models.DateTimeField(default=timezone.now)
