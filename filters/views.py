from django.shortcuts import render
from django.http import HttpResponse
from .models import Product, Brand, AgeGroup, ShoeType
# Create your views here.


def index(request):
    brand = Brand.objects.all()
    products = Product.objects.all()
    for_user = AgeGroup.objects.all()
    shoe_type = ShoeType.objects.all()
    return render(request, 'shoes/index.html', {'products': products, 'brand': brand, 'for_user': for_user, 'shoe_type': shoe_type})


def brand_details(request, brand_id):
    brand_logo = Brand.objects.get(id=brand_id)
    product = Product.objects.filter(brand_id=brand_id)
    return render(request, 'shoes/details.html', {'prod': product, "item": brand_logo})


def group_details(request, group_id):
    user_group = AgeGroup.objects.get(id=group_id)
    product = Product.objects.filter(age_group_id=group_id)
    return render(request, 'shoes/details.html', {'prod': product, "item": user_group})


def type_details(request, type_id):
    shoe_type = ShoeType.objects.get(id=type_id)
    product = Product.objects.filter(shoe_type_id=type_id)
    return render(request, 'shoes/details.html', {'prod': product, "item": shoe_type})
