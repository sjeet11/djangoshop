# Generated by Django 2.1 on 2019-06-21 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('filters', '0005_auto_20190621_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brand',
            name='logo',
            field=models.CharField(max_length=10000),
        ),
    ]
