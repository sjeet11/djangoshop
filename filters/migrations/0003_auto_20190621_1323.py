# Generated by Django 2.1 on 2019-06-21 07:53

from django.db import migrations, models


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('filters', '0002_product_time_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='img1_url',
            field=models.CharField(default='null', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='img2_url',
            field=models.CharField(default='null', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='img3_url',
            field=models.CharField(default='null', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='img4_url',
            field=models.CharField(default='null', max_length=255),
            preserve_default=False,
        ),
    ]
